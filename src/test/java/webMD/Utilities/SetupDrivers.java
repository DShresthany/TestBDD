package webMD.Utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import io.github.bonigarcia.wdm.WebDriverManager;

public class SetupDrivers {

	public static WebDriver chromeDriver;

	public static void setupChromeDriver() {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
        options.addArguments("headless");
        options.addArguments("window-size=1400,800");       
        options.addArguments("disable-gpu");
		chromeDriver = new ChromeDriver(options);
	}

	public static void closeChromeDriver() {
		chromeDriver.quit();
	}
}
